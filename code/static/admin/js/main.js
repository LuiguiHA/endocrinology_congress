function openModalDismissed(){
    $('#dismissed-modal').modal()
}
function openModalConfirmed(){
    $('#confirmed-modal').modal()
}
function abstractDismissed(id){
    var reason = $('#dismissed-reason').val()
    $('#button-dismissed').css('display', 'none')
    $('.message-loading').css('display', 'block')
    $.ajax({
        type:'GET',
        url:'/abstracts-dismissed/',
        data: {
            reason: reason,
            id: id
        },
        success: function(response){
            $('#button-dismissed').css('display', 'block')
            $('.message-loading').css('display', 'none')
            window.location = '/admin/abstract/abstracts/'
        }
    });
}
function abstractConfirmed(id){
    $('#button-confirmed').css('display', 'none')
    $('#close-confirmed').css('display', 'none')
    $('.message-loading').css('display', 'block')
    $.ajax({
        type:'GET',
        url:'/abstracts-confirm/',
        data: {
            id: id
        },
        success: function(response){
            $('#button-confirmed').css('display', 'inline-block')
            $('#close-confirmed').css('display', 'inline-block')
            $('.message-loading').css('display', 'none')
            window.location = '/admin/abstract/abstracts/'
        }
    });
}
$(document).ready(function(){
    document.getElementById('button-dismissed').disabled = true
});
$('#dismissed-reason').bind('input propertychange', function(){
    if ($('#dismissed-reason').val() == ''){
        document.getElementById('button-dismissed').disabled = true
    }
    else {
        document.getElementById('button-dismissed').disabled = false
    }
})