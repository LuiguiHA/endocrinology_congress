Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale("es")

var baseVue = new Vue({
  el: '#base',
  delimiters: ['${', '}'],
  data:{
    languageUrl: window.languageUrl,
    logoutUrl: window.logoutUrl
  },
  methods: {
    selectLanguage: function(language){
      axios.post(window.languageUrl,{
        language_code: language
      })
      .then(function(response){
        response = response.data
        if (response.success){
          location.reload(true);
        }
      })
    },
    logout: function(){
      console.log("logout", this.logoutUrl)
      axios.post(this.logoutUrl)
      .then(function(response){
        response = response.data
        if (response.success){
          window.location = "/abstracts-list";
        }
      })
    },
    clickHomeMenu: function(section,path){
			$('.navbar-collapse').removeClass('in');

			if (window.location.pathname =="/"){
				this.scrollToSection(section)
			}
			else{
				window.location = path
			}
    },
    hola: function(){
      console.log("=====")
    },
    scrollToSection: function(section){
			var extraY = $('nav').hasClass('smaller') ? 0 : 50
			$('body','html').scrollTo($(section).offset().top - extraY, 1000)
		}
  }
})
