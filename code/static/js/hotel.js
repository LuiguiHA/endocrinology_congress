// ELEMENT.locale(ELEMENT.lang.es)
Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
Vue.use(VueCarousel.default)
var card = window.VueCard.Card
VeeValidate.Validator.setLocale(window.LANGUAGE_CODE)
var PulseLoader = VueSpinner.PulseLoader
var app = new Vue({
    el: '#hotel',
    delimiters: ['${', '}'],
    components:{
      PulseLoader: PulseLoader,
      card: card
    },
    data: function(){
        return {
            _: window._,
            hotels: window.hotels,
            language: window.LANGUAGE_CODE,
            showImage: '',
            roomSelected: null,
            reservationRange: null,
            showPrice: '',
            showReservedDialog: false,
            showReservationResponseDialog: false,
            terms: false,
            loading: false,
            loadingPrice: false,
            cardDetail: this.informationCulqiFormEmpty(),
            inscriptionForm: this.informationInscriptionFormEmpty(),
            reservedHotelUrl: window.reservedHotelUrl,
            priceReservationUrl: window.priceReservationUrl,
            reservationResponse: {
                email: '',
                codeTransaction: '',
                dateTransaction: '',
                amount: '',
                hotelName: '',
                roomName: '',
                range: '',
                days: ''
              },
        }   
    },
    created: function(){
        Culqi.publicKey = window.CULQI_PUBLIC_KEY;
        Culqi.init();
        
        _.forEach(this.hotels, function(hotel){
            hotel.showImage = hotel.images[0].photo
        })
    },
    mounted: function(){
        this.getDatesValid()
        if (window.LANGUAGE_CODE == 'es'){
            window.ELEMENT.locale(window.ELEMENT.lang.es)
        }
        if (window.LANGUAGE_CODE == 'en'){
            window.ELEMENT.locale(window.ELEMENT.lang.en)
        }
        
    },
    computed: {
        cardMonth: function () {
            var dateSplitted = this.cardDetail.expiry.split("/")
      
            if(dateSplitted.length == 2){
              return dateSplitted[0].replace(/\s/g, '');
            }
            return '';
        },
        cardYear: function () {
            var dateSplitted = this.cardDetail.expiry.split("/")
            if(dateSplitted.length == 2){
                var year = dateSplitted[1].replace(/\s/g, '');
                year = "20" + year
                return year;
            }
            return '';
        }
    },
    methods:{
        getDatesValid: function(){
            return {
                disabledDate: function(date){
                    var minimunDate = moment('2018-10-20')
                    var maximumDate = moment('2018-10-30')
                    var isAfterSince = moment(date).isSameOrAfter(minimunDate)
                    var isBeforeUntil = moment(date).isSameOrBefore(maximumDate)
                    return !(isAfterSince && isBeforeUntil)
                }
            }
        },
        setShowImage: function(url, index){
            var hotel = this.hotels[index]
            hotel.showImage = url
            this.$set(this.hotels, index, hotel)
        },
        onBeforeClosePaymentDialog: function() {
            this.showReservedDialog = false;
        },
        onBeforeCloseReservationResponseDialog: function() {
            this.showReservationResponseDialog = false;
          },
        showModal: function(index){
            var roomSelected = this.hotels[index].roomSelected
            var room = this.hotels[index].rooms[roomSelected]
            var since = moment(this.hotels[index].reservationRange[0])
            var until = moment(this.hotels[index].reservationRange[1])
            var days = until.diff(since, 'days') + 1
            this.inscriptionForm.roomId = room.id
            this.inscriptionForm.since = since.format('YYYY-MM-DD')
            this.inscriptionForm.until = until.format('YYYY-MM-DD')
            this.inscriptionForm.days = days

            this.loadingPrice = true
            axios.get(this.priceReservationUrl, {
                params: {
                    id: room.id,
                    days: days
                }
            })
            .then(function(response){
                response = response.data
                this.showPrice = response.price
                this.loadingPrice = false
                this.showReservedDialog = true
            }.bind(this))
             
        },
        validateCulqi: function () {
            this.$validator.validateAll().then(function(result){
                if (result){
                    if (this.terms == false){
                        if(this.language == 'es'){
                            var errorTermsMessage =  "Para continuar debe aceptar las términos y condiciones"
                        }
                        if(this.language == 'en'){
                            var errorTermsMessage =  "To continue you must accept the terms and conditions"
                        }
                        new Noty({
                            type:"error",
                            text: errorTermsMessage,
                            layout: "top",
                            timeout: 5000,
                            force: true,
                            killer: true
                        }).show();
                        return;
                    }
                    this.loading = true;
                    Culqi.createToken();
                    window.culqi = function() {
                        if(Culqi.token) {
                            this.sendRegistration(Culqi.token)
                        } else {
                            this.loading = false;
                            if (this.language == 'es'){
                                var message = "Hubo un error al registrar la tarjeta"
                            }
                            if (this.language == 'en'){
                                var message = "There was an error registering the card"
                            }
                            switch(Culqi.error.code){
                            case "invalid_email":
                                if (this.language == 'es'){
                                    message = "El correo es inválido"
                                }
                                if (this.language == 'en'){
                                    message = "The email is invalid"
                                }
                            break;
            
                            case "invalid_number":
                                if (this.language == 'es'){
                                    message = "El número de tarjeta es inválido"
                                }
                                if (this.language == 'en'){
                                    message = "The card number is invalid"
                                }
                            break;
            
                            case "invalid_expiration_year":
                                if (this.language == 'es'){
                                    message = "El año de expiración es inválido"
                                }
                                if (this.language == 'en'){
                                    message = "The expiration year is invalid"
                                }
                            break;
            
                            case "invalid_expiration_month":
                                if (this.language == 'es'){
                                    message = "El mes de expiración es inválido"
                                }
                                if (this.language == 'en'){
                                    message = "The expiration month is invalid"
                                }
                            break;
                            }
                            new Noty({
                            type:"error",
                            text: message,
                            layout: "top",
                            timeout: 5000,
                            force: true,
                            killer: true
                            }).show();
                        }
                    }.bind(this)
                }
            }.bind(this));
        },
        sendRegistration: function (token) {
            axios.post(this.reservedHotelUrl, {
              'token': token,
              'client': this.inscriptionForm
            })
            .then(function(response){
              this.loading = false;
              response = response.data
              if (response.success){
                this.inscriptionForm = this.informationInscriptionFormEmpty()
                this.cardDetail = this.informationCulqiFormEmpty()
                this.$nextTick(function () {
                  this.errorsVee.clear();
                })
                this.showReservedDialog = false;
                reservation = response.reservation
                this.paymentReservationResponse(reservation)
              } else {
                new Noty({
                  type:"error",
                  text: response.message,
                  layout: "top",
                  timeout: 5000,
                  force: true,
                  killer: true
                }).show();
              }
            }.bind(this))
        },
        paymentReservationResponse: function(reservation){
            this.reservationResponse.email = reservation.email
            this.reservationResponse.amount = reservation.amount
            this.reservationResponse.codeTransaction = reservation.transaction_id
            this.reservationResponse.dateTransaction = reservation.transaction_date
            this.reservationResponse.hotelName = reservation.hotel_name
            this.reservationResponse.roomName = reservation.room_name
            if (reservation.since == reservation.until){
                this.reservationResponse.range = reservation.since
            } else{
                this.reservationResponse.range = reservation.since + ' - ' + reservation.until
            }
            this.reservationResponse.days = reservation.days
            this.showReservationResponseDialog = true
        },
        informationCulqiFormEmpty: function (){
            return {
              number: '',
              expiry: '',
              cvc: '',
            }
        },
        informationInscriptionFormEmpty: function () {
            return {
              name: '',
              lastName: '',
              document: '',
              phone: '',
              email: '',
              documentType: 1,
              since: '',
              until: '',
              days: '',
              roomId: null,
            }
        },
    }
})