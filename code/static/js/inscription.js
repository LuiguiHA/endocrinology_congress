Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale(window.LANGUAGE_CODE)
var PulseLoader = VueSpinner.PulseLoader
var card = window.VueCard.Card

var app = new Vue({
  el: '#inscription',
  delimiters: ['${', '}'],
  components:{
    PulseLoader: PulseLoader,
    card: card,
  },
  data: function () {
    return {
      countriesUrl: window.countriesUrl,
      registerUserUrl: window.registerUserUrl,
      partnerResponseUrl: window.partnerResponseUrl,
      paymentResponseUrl: window.paymentResponseUrl,
      cardNumber: '',
      cardDate: '',
      cardCvv: '',
      terms: false,
      cardDetail: this.informationCulqiFormEmpty(),
      inscriptionForm: this.informationInscriptionFormEmpty(),
      participantResponse: {
        email: '',
        codeTransaction: '',
        dateTransaction: '',
        amount: '',
      },
      loginFail: false,
      loading: false,
      loadingForm: false,
      registerSuccess: '',
      showPaymentDialog: false,
      showParticipantResponseDialog: false,
      countries : [],
      amount: ''
    }
  },
  created: function () {
      Culqi.publicKey = window.CULQI_PUBLIC_KEY;
      Culqi.init();

      axios.get(this.countriesUrl)
      .then(function(response){
        response = response.data
        if (response.success){
          this.countries = response.countries
        }
      }.bind(this))
  },
  computed: {
    cardMonth: function () {
      var dateSplitted = this.cardDetail.expiry.split("/")

      if(dateSplitted.length == 2){
        return dateSplitted[0].replace(/\s/g, '');
      }
      return '';
    },
    cardYear: function () {
      var dateSplitted = this.cardDetail.expiry.split("/")
      if(dateSplitted.length == 2){
        var year = dateSplitted[1].replace(/\s/g, '');
        year = "20" + year
        return year;
      }
      return '';
    },
  },
  methods: {
    paymentResponse: function(){
      axios.post(this.paymentResponseUrl,{
        'isPartner': this.inscriptionForm.isPartner
      })
      .then(function(response){
        response = response.data
        this.amount = response.amount
      }.bind(this))
    },
    openModal: function (scope) {
      this.$validator.validateAll(scope).then(function(result){
        if (result){
          this.loadingForm = true
          axios.post(this.partnerResponseUrl,{
              'name': this.inscriptionForm.name,
              'lastName': this.inscriptionForm.lastName
          })
          .then(function(response){
            response = response.data
            if (response.isPartner){
              if(response.used){
                this.loadingForm = false;
                new Noty({
                  type:"error",
                  text: "Éste socio ya se ha registrado.",
                  layout: "top",
                  timeout: 5000,
                  force: true,
                  killer: true
                }).show();
              } else {
                this.inscriptionForm.isPartner = true;
                this.paymentResponse()
                this.loadingForm = false;
                this.showPaymentDialog = true;
              }

            }
            else{
              this.inscriptionForm.isPartner = false;
              this.paymentResponse()
              this.loadingForm = false
              this.showPaymentDialog = true;
            }
          }.bind(this));
        }
      }.bind(this));
    },
    onBeforeClosePaymentDialog: function() {
      this.showPaymentDialog = false;
    },
    onBeforeCloseParticipantResponseDialog: function() {
      this.showParticipantResponseDialog = false;
    },
    onOpenPaymentDialog: function () {

    },
    informationInscriptionFormEmpty: function () {
      return {
        name: '',
        lastName: '',
        document: '',
        document_type: '',
        phone: '',
        profession: '',
        speciality: '',
        country: '',
        city: '',
        address: '',
        email: '',
        password: '',
        isPartner: false,
        code: null,
      }
    },
    informationCulqiFormEmpty: function () {
      return {
        number: '',
        expiry: '',
        cvc: '',
      }
    },
    paymentParticipantResponse: function (participant) {
      this.participantResponse.email = participant.email
      this.participantResponse.amount = participant.amount
      this.participantResponse.codeTransaction = participant.transaction_id
      this.participantResponse.dateTransaction = participant.transaction_date
      this.showParticipantResponseDialog = true
    },
    sendRegistration: function (token) {
      axios.post(this.registerUserUrl, {
        'token': token,
        'participant': this.inscriptionForm
      })
      .then(function(response){
        this.loading = false;
        response = response.data
        if (response.success){
          this.inscriptionForm = this.informationInscriptionFormEmpty()
          this.cardDetail = this.informationCulqiFormEmpty()
          this.$nextTick(function () {
            this.errorsVee.clear('inscription');
            this.errorsVee.clear('culqi');
          })
          this.showPaymentDialog = false;
          participant = response.participant
          this.paymentParticipantResponse(participant)
        } else {
          new Noty({
            type:"error",
            text: response.message,
            layout: "top",
            timeout: 5000,
            force: true,
            killer: true
          }).show();
        }
      }.bind(this))
    },
    validateCulqi: function (scope) {

      this.$validator.validateAll(scope).then(function(result){
        if (result){
          if (this.terms == false){
            var errorTermsMessage = "Para continuar debe aceptar las términos y condiciones"
              new Noty({
              type:"error",
              text: errorTermsMessage,
              layout: "top",
              timeout: 5000,
              force: true,
              killer: true
            }).show();
            return;
          }
          this.loading = true;
          Culqi.createToken();
          window.culqi = function() {
            if(Culqi.token) {
              this.sendRegistration(Culqi.token)
            } else {
              this.loading = false;
              var message = "Hubo un error al registrar la tarjeta"
              switch(Culqi.error.code){
                case "invalid_email":
                message = "El email es inválido"
                break;

                case "invalid_number":
                message = "El número de tarjeta es inválido"
                break;

                case "invalid_expiration_year":
                message = "El año de expiración es inválido"
                break;

                case "invalid_expiration_month":
                message = "El mes de expiración es inválido"
                break;
              }
              new Noty({
                type:"error",
                text: message,
                layout: "top",
                timeout: 5000,
                force: true,
                killer: true
              }).show();
            }
          }.bind(this)
       }
      }.bind(this));
    },
  }
})
