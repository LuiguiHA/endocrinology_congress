Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale(window.LANGUAGE_CODE)
var PulseLoader = VueSpinner.PulseLoader
Vue.use(window.VueQuillEditor)
var app = new Vue({
  el: '#abstracts',
  delimiters: ['${', '}'],
  components:{
    PulseLoader: PulseLoader,
  },
  data:{
    config: {
      toolbar: [
        [ 'Bold', 'Italic', 'Underline', 'Strike', 'Table' ]
      ],
      height: 300,
      language: window.LANGUAGE_CODE
    },
    currentAbstract: window.currentAbstract,
    userWithAbstract: window.userWithAbstract,
    countriesUrl: window.countriesUrl,
    AbstractListUrl: window.AbstractListUrl,
    saveAbstractUrl: window.saveAbstractUrl,
    typesAbstractUrl: window.typesAbstractUrl,
    categoriesAbstractUrl: window.categoriesAbstractUrl,
    // stateAuthorAbstractUrl: window.stateAuthorAbstractUrl,
    registerUrl: window.registerUrl,
    loginUrl: window.loginUrl,
    author:{
      name: "",
      email: "",
      lastName: "",
      password: ""
    },
    login:{
      email: "",
      password: "",
    },
    abstract:{
      title: '',
      authors: [],
      info: '',
      approval: false,
      helsinki: false,
      animal: false,
      pacient: false,
      ilegal: false,
      previousPublication: false,
      previousSubmission: false,
      copyright: false,
      copyrightCanceled: false,
      guidelines: false,
      conflict: false,
      conflictReason: '',
      funding: false,
      fundingReason: '',
      category: '',
      type: '',
      clinicalCase: false,
      presentation: null,
      // stateAuthor: '',
      // birthAuthor: '',
      // grant: null,
      // grantEspe: false,
      // member: null
    },
    countries:[],
    categories: [],
    types: [],
    // states: [],
    loginFail: false,
    loading: false,
    loadingAbstract: false,
    registerSuccess: "",
    readOnly: false,
    titleError: false,
    fundingError: false,
    conflictError: false,
    abstractError: false,
    quillOptions: {
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent

          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

          [{ 'align': [] }]
        ]
      },
      readOnly: false,
      placeholder: 'Ingresa tu texto aquí...'
    },
  },
  created: function(){
    if(window.LANGUAGE_CODE == 'en'){
      this.quillOptions.placeholder = 'Enter your text here..'
    }
    if (this.currentAbstract){
      this.quillOptions.readOnly = true
    }
    axios.get(this.countriesUrl)
    .then(function(response){
      response = response.data
      if (response.success){
        this.countries = response.countries
      }
    }.bind(this))
    axios.get(this.categoriesAbstractUrl)
    .then(function(response){
      response = response.data
      if (response.success){
        this.categories = response.categories
      }
    }.bind(this))
    axios.get(this.typesAbstractUrl)
    .then(function(response){
      response = response.data
      if (response.success){
        this.types = response.types
      }
    }.bind(this))
    // axios.get(this.stateAuthorAbstractUrl)
    // .then((response) => {
    //   response = response.data
    //   if (response.success){
    //     this.states = response.states
    //   }
    // })
  },
  computed: {
    titleWords: function(){
      if(this.abstract.title.match(/\S+/g) == null){
        return window.LANGUAGE_CODE == 'es' ? '0 PALABRAS' : '0 WORDS'
      }
      var wordsString = this.abstract.title.replace(/<(?:[^>=]|='[^']*'|="[^"]*"|=[^'"][^\s>]*)*>/g, "")
      var words = wordsString.match(/\S+/g)
      if (words.length > 50){
        this.titleError = true
      } else{
        this.titleError = false
      }
      return window.LANGUAGE_CODE == 'es' ? String(words.length) + ' PALABRAS.' : String(words.length) + ' WORDS.'
    },
    abstractWords: function(){
      if(this.abstract.info.match(/\S+/g) == null){
        return window.LANGUAGE_CODE == 'es' ? '0 PALABRAS' : '0 WORDS'
      }
      var wordsString = this.abstract.info.replace(/<(?:[^>=]|='[^']*'|="[^"]*"|=[^'"][^\s>]*)*>/g, "")
      var words = wordsString.match(/\S+/g)
      if (words.length > 400000){
        this.abstractError = true
      } else{
        this.abstractError = false
      }
      return window.LANGUAGE_CODE == 'es' ? String(words.length) + ' PALABRAS.' : String(words.length) + ' WORDS.'
    },
    conflictReasonWords: function(){
      if(this.abstract.conflictReason.match(/\S+/g) == null){
        return window.LANGUAGE_CODE == 'es' ? '0 PALABRAS' : '0 WORDS'
      }
      var wordsString = this.abstract.conflictReason.replace(/<(?:[^>=]|='[^']*'|="[^"]*"|=[^'"][^\s>]*)*>/g, "")
      var words = wordsString.match(/\S+/g)
      if (words.length > 50){
        this.conflictError = true
      } else{
        this.conflictError = false
      }
      return window.LANGUAGE_CODE == 'es' ? String(words.length) + ' PALABRAS.' : String(words.length) + ' WORDS.'
    },
    fundingReasonWords: function(){
      if(this.abstract.fundingReason.match(/\S+/g) == null){
        return window.LANGUAGE_CODE == 'es' ? '0 PALABRAS' : '0 WORDS'
      }
      var wordsString = this.abstract.fundingReason.replace(/<(?:[^>=]|='[^']*'|="[^"]*"|=[^'"][^\s>]*)*>/g, "")
      var words = wordsString.match(/\S+/g)
      if (words.length > 50){
        this.fundingError = true
      } else{
        this.fundingError = false
      }
      return window.LANGUAGE_CODE == 'es' ? String(words.length) + ' PALABRAS.' : String(words.length) + ' WORDS.'
    }
  },
  mounted: function(){
    if (this.currentAbstract){
      this.readOnly = true
      this.abstract.title = this.currentAbstract.title
      this.abstract.info = this.currentAbstract.info
      this.abstract.approval = this.currentAbstract.approval
      this.abstract.helsinki = this.currentAbstract.helsinki
      this.abstract.animal = this.currentAbstract.animal
      this.abstract.pacient = this.currentAbstract.pacient
      this.abstract.ilegal = this.currentAbstract.ilegal
      this.abstract.previousPublication = this.currentAbstract.previousPublication
      this.abstract.previousSubmission = this.currentAbstract.previousSubmission
      this.abstract.copyright = this.currentAbstract.copyright
      this.abstract.copyrightCanceled = this.currentAbstract.copyrightCanceled
      this.abstract.guidelines = this.currentAbstract.guidelines
      this.abstract.conflict = this.currentAbstract.conflict
      this.abstract.conflictReason = this.currentAbstract.conflictReason
      this.abstract.funding = this.currentAbstract.funding
      this.abstract.fundingReason = this.currentAbstract.fundingReason
      this.abstract.category = this.currentAbstract.category
      this.abstract.type = this.currentAbstract.type
      // this.abstract.stateAuthor = this.currentAbstract.stateAuthor
      this.abstract.clinicalCase = this.currentAbstract.clinicalCase
      this.abstract.presentation = this.currentAbstract.presentation
      // this.abstract.birthAuthor = this.currentAbstract.birthAuthor
      // this.abstract.grant = this.currentAbstract.grant
      // this.abstract.grantEspe = this.currentAbstract.grantEspe
      // this.abstract.member = this.currentAbstract.member
      this.abstract.authors = this.currentAbstract.authors
      
    }else{
      this.abstract.authors.push({
        name: '',
        lastName: '',
        presentation: false,
        affiliations: [{
          institution: '',
          city: '',
          country: ''
        }]
      })
    }
  },
  methods: {
    registerAuthor: function(){
      this.registerSuccess = "";
      this.$validator.validateAll().then(function(result){
        if (result){
          this.loading = true
          axios.post(this.registerUrl,this.author)
          .then(function(response){
            this.loading = false
            response = response.data
            if (response.success){
              this.author.name = ""
              this.author.email = ""
              this.author.lastName = ""
              this.author.password = ""
              this.$nextTick(function(){
                this.errorsVee.remove("password-register")
                this.errorsVee.remove("email-register")
                this.errorsVee.remove("last_name")
                this.errorsVee.remove("name")
              }.bind(this))
              this.registerSuccess = true;
            }
            else{
              this.registerSuccess = false;
            }
          }.bind(this))
        }
      }.bind(this));
    },
    loginUser: function(){
      this.loginFail = false;

      this.$validator.validateAll().then(function(result){
        if (result){
          loading = true
          axios.post(this.loginUrl, this.login)
          .then(function(response){
            loading = false
            response = response.data
            if (response.success){
              window.location = "/abstracts-list";
            }
            else{
              this.loginFail = true;
            }
          }.bind(this))
        }
      }.bind(this));
    },
    addAuthor: function(){
      this.abstract.authors.push({
        name: '',
        lastName: '',
        presentation: false,
        affiliations: [{
          institution: '',
          city: '',
          country: ''
        }]
      })
    },
    addAffiliation: function(index){
      var author = this.abstract.authors[index]
      author.affiliations.push({
        institution: '',
        city: '',
        country: ''
      })
    },
    deleteAuthor: function(index){
      this.abstract.authors.splice(index,1)
    },
    deleteAffiliation: function(index, indexAffiliation){
      var author = this.abstract.authors[index]
      author.affiliations.splice(indexAffiliation, 1)
    },
    saveAbstract: function(){
      this.$validator.validateAll().then(function(result){
        if(result){
          if (this.titleError || this.abstractError || this.conflictError || this.fundingError){
            this.$message.error('Oops, Existen campos con más palabras de lo permitido.');
            return
          }
          this.loadingAbstract = true
          axios.post(this.saveAbstractUrl, this.abstract)
          .then(function(response){
            response = response.data
            if (response.success){
              this.loadingAbstract = false
              window.location = this.AbstractListUrl
            }
          }.bind(this))
        }
        else{
          this.$message.error('Oops, Existen campos con errores.');
          return
        }
      }.bind(this))
    },
    showAbstract: function(id){
      window.location = this.saveAbstractUrl + '?id=' + id
    },
    backAbstractList: function(){
      window.location = this.AbstractListUrl
    }
  }
})
