"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from nested_admin import urls
from django.conf.urls.static import static
from apps.registrados.views import RegisterView, ProgramView, \
     InscriptionView, LanguageView, CountriesView, IndexView, \
     PartnerResponseView, PaymentResponseView, VenueView
from apps.abstract.views import AbstractListView, Activate, LoginView, \
    LogoutView, RegisterAuthorView, AbstractView, CategoriesAbstractView, TypesAbstractView, \
    StateAuthorAbstractView, AbstractConfirmView, AbstractDismissedView, DownloadAbstractsView
from django.views.generic import TemplateView

from apps.hoteles.views import HotelsView, HotelDetailView, HotelReservedView, PriceReservedView

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^program/', ProgramView.as_view()),
    url(r'^inscription/', InscriptionView.as_view()),
    url(r'^abstracts-list/', AbstractListView.as_view(), name='abstract_list'),
    url(r'^abstracts-confirm/', AbstractConfirmView.as_view(), name='abstract_confirm'),
    url(r'^abstracts-dismissed/', AbstractDismissedView.as_view(), name='abstract_dismissed'),
    url(r'^abstract-register/', AbstractView.as_view(), name='abstract'),
    url(r'^categories-abstract/', CategoriesAbstractView.as_view(), name='categories_abstract'),
    url(r'^types-abstract/', TypesAbstractView.as_view(), name='types_abstract'),
    url(r'^states-author-abstract/', StateAuthorAbstractView.as_view(), name='states_author_abstract'),
    url(r'^venue/', VenueView.as_view(), name='venue'), 
    url(r'^contact/',  TemplateView.as_view(template_name='contact.html'), name='contact'),
    url(r'^summer-school-2018/',  TemplateView.as_view(template_name='summer.html'), name='contact'),
    url(r'^hotels/', HotelsView.as_view(), name='hotels'),
    url(r'^hotel-detail/(?P<slug>[-\w]+)/$', HotelDetailView.as_view(), name='hotel_detail'),
    url(r'^hotel-reserved/', HotelReservedView.as_view(), name='hotel_reserved'),
    url(r'^price-reserved/', PriceReservedView.as_view(), name='price_reserved'),
    url(r'^registeruser/', RegisterView.as_view(), name='registeruser'),
    url(r'^register/', RegisterAuthorView.as_view(), name='register'),
    url(r'^login/', LoginView.as_view(), name='login'),
    url(r'^logout/', LogoutView.as_view(), name='logout'),
    url(r'^language/', LanguageView.as_view(), name='language'),
    url(r'^countries/', CountriesView.as_view(), name='countries'),
    url(r'^partner/', PartnerResponseView.as_view(), name='partner'),
    url(r'^payment/', PaymentResponseView.as_view(), name='payment'),
    url(r'^download-abstracts/', DownloadAbstractsView.as_view(), name='download_abstracts'),
    url(r'^nested_admin/', include('nested_admin.urls')),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        Activate.as_view(), name='activate'),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
