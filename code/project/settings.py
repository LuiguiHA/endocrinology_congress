"""
Django settings for project project.

Generated by 'django-admin startproject' using Django 1.11.3.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')x5mecpa-d0v_5!$ziw9$ibdt5v7c(wo3ciu6*t3x$8!p@98du'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'anymail',
    'apps.registrados',
    'apps.abstract',
    'apps.hoteles',
    'nested_admin'
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware'
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates/'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
                'django.template.context_processors.i18n',
                'apps.registrados.context_processors.debug',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': os.environ.get('PGPORT', '5432'),
        'ATOMIC_TRANSACTIONS': True
    },
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
            'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

INTERNAL_IPS = (
    '0.0.0.0',
    '127.0.0.1',
)

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'es'
LANGUAGE_SESSION_KEY = LANGUAGE_CODE
LANGUAGES = (
 ('es', _('Spanish')),
 ('en', _('English')),
 ('pr', _('Portuguese')),
)

TIME_ZONE = 'America/Lima'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

SUIT_CONFIG = {
    'ADMIN_NAME': 'Administrador',
    'HEADER_DATE_FORMAT': 'l, j F Y',
}


# ANYMAIL = {
#     "MAILGUN_API_KEY": "key-0zc5pdav-olhaclwgh9je74j0mtany61",
#     "MAILGUN_SENDER_DOMAIN": 'exploor.review',
# }

# outlook.office365.com 993 ssl

EMAIL_HOST = 'smtp.office365.com'
EMAIL_HOST_USER = 'info@slep2018.com'
EMAIL_HOST_PASSWORD = 'WebSLEP2018+'
EMAIL_PORT = 587
# EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
EMAIL_USE_TLS = True 
# EMAIL_USE_SSL = True
DEFAULT_FROM_EMAIL = "info@slep2018.com"
# DEFAULT_FROM_EMAIL = "postmaster@exploor.review"
OPERATOR_EMAIL = "orientours@hotmail.com"

LOGIN_URL = "/"

AUTHENTICATION_BACKENDS = [
    'project.backends.EmailBackend',
]

SOCIAL_AUTH_URL_NAMESPACE = 'social'

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'project.pipeline.get_user_data',
)

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_KEY = os.environ.get('SOCIAL_AUTH_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = os.environ.get('SOCIAL_AUTH_FACEBOOK_SECRET')
SOCIAL_AUTH_FACEBOOK_API_VERSION = '2.10'

SOCIAL_AUTH_TWITTER_KEY = os.environ.get('SOCIAL_AUTH_TWITTER_KEY')
SOCIAL_AUTH_TWITTER_SECRET = os.environ.get('SOCIAL_AUTH_TWITTER_SECRET')

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.environ.get('SOCIAL_AUTH_GOOGLE_OAUTH2_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.environ.get(
                            'SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET')

SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = os.environ.get(
                            'SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY')
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = os.environ.get(
                            'SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET')
SOCIAL_AUTH_LINKEDIN_SCOPE = ['r_basicprofile', 'r_emailaddress']
FIELD_SELECTORS = ['email-address', ]
SOCIAL_AUTH_LINKEDIN_FIELD_SELECTORS = ['email-address']
SOCIAL_AUTH_LINKEDIN_EXTRA_DATA = [('id', 'id'),
                                   ('firstName', 'first_name'),
                                   ('lastName', 'last_name'),
                                   ('emailAddress', 'email'), ]

SOCIAL_AUTH_LOGIN_REDIRECT_URL = "/"
SOCIAL_AUTH_LOGIN_ERROR_URL = '/'

HTTP_401_UNAUTHORIZED = 'TOKEN INVALIDO'

LOGGING = {
   "version": 1,
   "disable_existing_loggers": False,
   "handlers": {
       "file": {
           "level": "INFO",
           "class": "logging.FileHandler",
           "filename": "debug.log",
       },
       "console": {
           "level": "INFO",
           "class": "logging.StreamHandler",
       },
   },
   "loggers": {
       "django.db.backends": {
        "handlers": ["console"],
        "level": "INFO",
       },
       "django": {
           "handlers": ["file", "console"],
           "level": "INFO",
           "propagate": True,
       },
       "users.admin": {
           "handlers": ["console"],
           "level": "INFO",
       },
       "django.template": {
           "handlers": ["console"],
           "level": "INFO",
           "propagate": False,
       },
   },
}
