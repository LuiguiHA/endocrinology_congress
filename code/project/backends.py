from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
            if getattr(user, 'is_active', True) and user.check_password(password):
                print ("user")
                return user
            else:
                print ("not user")
                return None
        except UserModel.DoesNotExist:
            print("=======")
            return None
