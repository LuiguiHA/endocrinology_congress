from .models import PlanEvent, RegisterPaymentInfo, Participant, \
                    City, Country, Partners
from django.http import JsonResponse
from datetime import datetime, date
import json
import culqipy
import arrow
import random
from dateutil import tz
from hashids import Hashids
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.translation import LANGUAGE_SESSION_KEY


def AmountResponse(is_partner):
    if is_partner:
        return 250
    else:
        plan_event = PlanEvent.objects.get(is_partner=is_partner)
        now_date = arrow.now('America/Lima').date()
        if now_date <= plan_event.early_1_date:
            return plan_event.price_1
        elif plan_event.early_1_date < now_date <= plan_event.early_2_date:
            return plan_event.price_2


def ParticipantResponse(participant):

    register = RegisterPaymentInfo.objects.get(registered=participant)
    participant_dict = {
        'email': participant.email,
        'amount': '$' + '{:.2f}'.format(register.amount),
        'transaction_id': register.transaction_id,
        'transaction_date': arrow.get(
            register.transaction_date, tz.gettz('America/Lima')
            ).shift(hours=-5).format('YYYY-MM-DD HH:mm:ss')
    }
    return participant_dict


def EmailConfirmation(participant, confirmation_code, request):
    language = request.LANGUAGE_CODE
    email = participant.email

    email_template = render_to_string(
        "emails/confirm_payment_{}.html".format(language),
        {
            'participant': participant,
            'confirmation_code': confirmation_code,
        }
    )

    subject = 'Confirmación de registro al XXVII Congreso Latinoamericano de Endocrinología Pediátrica'

    if language == "en":
        subject = 'Registration confirmation to XXVII Latin American Congress of Pediatric Endocrinology'
    try:
        send_mail(
            subject,
            '',
            settings.DEFAULT_FROM_EMAIL,
            [email],
            fail_silently=False,
            html_message=email_template,
        )
    except:
        pass


def ValidateCulqi(token, participant, request):
    isPartner = participant.get('isPartner')
    amount = AmountResponse(isPartner)
    country_id = participant.get('country')
    country = Country.objects.get(id=country_id)
    city = participant.get('city')
    

    charge_dict = {
        'card_brand': token.get('card_brand'),
        'amount': int(amount * 100),
        'currency_code': 'USD',
        'description': 'Registro',
        'email': token.get('email'),
        'installments': 0,
        'source_id': token.get('id'),
        'antifraud_details': {
            'address' : participant.get('address'),
            'address_city' : city,
            'country_code' : country.code,
            'first_name' : participant.get('name'),
            'last_name' : participant.get('lastName'),
            'phone_number' : str(participant.get('phone'))
        },
        'metadata': {
            'Profesión': participant.get('profession'),
            'Especialidad': participant.get('speciality'),
            'País': country.name,
            'Documento': participant.get('document'),
            'Tipo de documento': participant.get('document_type')
        }
    }

    charge = culqipy.Charge.create(charge_dict)
    if charge.get('object') == 'charge':
        participant_object = Participant()
        name = participant.get('name')
        name = name.upper().strip()
        participant_object.first_name = name.capitalize()
        last_name = participant.get('lastName')
        last_name = last_name.upper().strip()
        participant_object.last_name = last_name.capitalize()
        participant_object.phone = str(participant.get('phone'))
        participant_object.country = country.name
        participant_object.city = city
        participant_object.email = participant.get('email')
        participant_object.document = participant.get('document')
        participant_object.document_type = participant.get('document_type')
        participant_object.address = participant.get('address')
        participant_object.profession = participant.get('profession')
        participant_object.speciality = participant.get('speciality')
        participant_object.is_partner = participant.get('isPartner')
        if participant.get('isPartner'):
            partner = Partners.objects.get(name=name, last_name=last_name)
            partner.used = True
            partner.save()

        hashids = Hashids(min_length=6)
        confirmation_code = hashids.encode(random.randrange(0, 101, 2)).upper()
        participant_object.confirmation_code = confirmation_code
        participant_object.culqi_response = charge
        participant_object.save()
        #
        register = RegisterPaymentInfo()
        register.amount = amount
        register.transaction_id = charge.get('id')
        creation_date = int(charge.get('creation_date')) / 1000
        creation_date = datetime.fromtimestamp(creation_date).strftime('%Y-%m-%d %H:%M:%S')
        register.transaction_date = creation_date
        register.card_number = charge.get('source').get('card_number')
        register.card_number_last_four = charge.get('source').get('last_four')
        register.card_country = charge.get('source').get('client').get('ip_country')
        register.card_country_code = charge.get('source').get('client').get('ip_country_code')
        register.card_brand = charge.get('source').get('iin').get('card_brand')
        register.card_type = charge.get('source').get('iin').get('card_type')
        register.card_category = charge.get('source').get('iin').get('card_category')
        register.card_bank = charge.get('source').get('iin').get('issuer').get('name')
        register.card_bank_country = charge.get('source').get('iin').get('issuer').get('country')
        register.card_bank_country_code = charge.get('source').get('iin').get('issuer').get('country_code')
        register.client_ip = charge.get('source').get('client').get('ip')
        register.registered = participant_object
        register.save()

        # EMAIL CONFIRMATION
        EmailConfirmation(participant_object, confirmation_code, request)
        participant_response = ParticipantResponse(participant_object)
        return {'success': True, 'participant': participant_response}

    else:
        message = "Error en el pago (code: 1000)"
        if charge.get('type') == 'parameter_error':
            message = "Error de parámetro de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1001)"

        if charge.get('type') == 'invalid_request_error':
            message = "Ocurrió un error inesperado. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1002)"

        if charge.get('type') == 'authentication_error':
            message = "Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1003)"

        if charge.get('type') == 'limit_api_error':
            message = "Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)"

        if charge.get('type') == 'resource_error':
            message = "Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)"

        if charge.get('type') == 'api_error':
            message = "Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1007)"

        if charge.get('type') == 'card_error':

            if charge.get('decline_code'):

                if charge.get('decline_code') == 'insufficient_funds':
                    message = "Fondos insuficientes. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2001)"

                if charge.get('decline_code') == 'expired_card':
                    message = "Tarjeta vencida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2002)"

                if charge.get('decline_code') == 'stolen_card':
                    message = "La tarjeta ha sido reportada como perdida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2004)"

                if charge.get('decline_code') == 'contact_issuer':
                    message = "El banco ha rechazado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2005)"

                if charge.get('decline_code') == 'invalid_cvv':
                    message = "Código CVV inválido. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2006)"

                if charge.get('decline_code') == 'incorrect_cvv':
                    message = "Código CVV incorrecto. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2007)"

                if charge.get('decline_code') == 'too_many_attempts_cvv':
                    message = "Demasiados intentos para ingresar el código CVV. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2008)"

                if charge.get('decline_code') == 'issuer_not_available':
                    message = "El banco no responde, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2009)"

                if charge.get('decline_code') == 'issuer_decline_operation':
                    message = "El banco ha denegado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2010)"

                if charge.get('decline_code') == 'invalid_card':
                    message = "La tarjeta no está permitida para este tipo de transacciones, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2011)"

                if charge.get('decline_code') == 'processing_error':
                    message = "Ocurrió un error inesperado mientras se procesaba el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2012)"

                if charge.get('decline_code') == 'fraudulent':
                    message = "Transacción sospechosa, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2013)"

                if charge.get('decline_code') == 'culqi_card':
                    message = "Tarjeta no permitida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2014)"

            if charge.get('code'):

                if charge.get('code') == 'card_declined':
                    message = "Tarjeta rechazada. (code: 2000)"
                else:
                    message = "Ocurrío un error con la tarjeta (code: 2015)"

        return {'success': False, 'message': message}
