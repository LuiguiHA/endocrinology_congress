from django.db import models
from django.contrib.postgres.fields import JSONField
from apps.hoteles.models import HotelReservation


class PlanEvent(models.Model):

    price_1 = models.FloatField()

    price_2 = models.FloatField()

    early_1_date = models.DateField()

    early_2_date = models.DateField()

    is_partner = models.BooleanField()

    def __str__(self):
        return self.name


class Partners(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name="Nombres"
    )
    last_name = models.CharField(
        max_length=100,
        verbose_name="Apellidos"
    )
    used = models.BooleanField(
        default=False
    )


class Participant(models.Model):

    first_name = models.CharField(
        max_length=100,
        verbose_name="Nombres"
    )
    last_name = models.CharField(
        max_length=100,
        verbose_name="Apellidos",
        null=True
    )
    country = models.CharField(
        max_length=100,
        verbose_name="País"
    )
    city = models.CharField(
        max_length=100,
        verbose_name="Ciudad"
    )
    document = models.CharField(
        max_length=15,
        verbose_name="DNI o Pasaporte"
    )

    DOCUMENT_TYPES = (
        ('1', 'DNI'),
        ('2', 'PASAPORTE'),
    )
    document_type = models.CharField(
        max_length=1,
        verbose_name="Tipo de documento",
        choices=DOCUMENT_TYPES,
        default=DOCUMENT_TYPES[0][0],
    )

    email = models.EmailField(
        verbose_name="Correo"
    )
    phone = models.CharField(
        max_length=20,
        verbose_name="Teléfono"
    )
    profession = models.CharField(
        max_length=100,
        verbose_name="Profesión"
    )
    speciality = models.CharField(
        max_length=100,
        verbose_name="Especialidad",
        null=True
    )
    address = models.CharField(
        max_length=100,
        verbose_name="Dirección",
        null=True
    )
    confirmation_code = models.CharField(
        max_length=100,
        verbose_name="Código de registro",
        null=True
    )
    is_partner = models.BooleanField(
        max_length=100,
        verbose_name="Socio",
        default=False
    )

    culqi_response = JSONField()

    created_at = models.DateTimeField(
        auto_now_add=True,
        editable=False
    )

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta():
        verbose_name = "Participante"
        verbose_name_plural = "Participantes"


class RegisterPaymentInfo(models.Model):

    amount = models.IntegerField()
    transaction_id = models.CharField(
        max_length=100,
        verbose_name="Código de transacción"
    )
    transaction_date = models.DateTimeField(
        verbose_name="Fecha de transacción"
    )
    card_number = models.CharField(
        max_length=100,
        verbose_name="Número de tarjeta"
    )
    card_number_last_four = models.CharField(
        max_length=4,
        verbose_name="Últimos 4 dígitos"
    )
    card_country = models.CharField(
        max_length=100,
        verbose_name="País de tarjeta"
    )
    card_country_code = models.CharField(
        max_length=100,
        verbose_name="Código del país de tarjeta"
    )
    card_brand = models.CharField(
        max_length=100,
        verbose_name="Marca de tarjeta"
    )
    card_type = models.CharField(
        max_length=100,
        verbose_name="Tipo de tarjeta"
    )
    card_category = models.CharField(
        null=True,
        max_length=100,
        verbose_name="Categoría de tarjeta"
    )
    card_bank = models.CharField(
        max_length=100,
        verbose_name="Banco de tarjeta",
        null=True
    )
    card_bank_country = models.CharField(
        max_length=100,
        verbose_name="País del banco de tarjeta",
        null=True
    )
    card_bank_country_code = models.CharField(
        max_length=100,
        verbose_name="Código del país del banco de tarjeta",
        null=True

    )
    client_ip = models.CharField(
        max_length=100,
        verbose_name="IP del registrado"
    )
    registered = models.ForeignKey(
        Participant,
        null=True
    )
    reservation = models.ForeignKey(
        HotelReservation,
        null=True
    )

    def get_price(self):
        return '$' + '{:.2f}'.format(self.amount) if self.amount else ''
    get_price.short_description = 'Precio'

    def category_name(self):
        if self.card_category:
            return self.card_category
        else:
            return '-'
    category_name.short_description = 'Categoría de tarjeta'

    def __str__(self):
        return ""

    class Meta():
        verbose_name = "Registro de pago"
        verbose_name_plural = "Registros de pago"


class Country(models.Model):
    name = models.CharField(
        max_length=100
    )
    code = models.CharField(
        max_length=2,
        unique=True
    )


class City(models.Model):
    name = models.CharField(
        max_length=100
    )
    country = models.ForeignKey(
        Country
    )
