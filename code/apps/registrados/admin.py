from django.contrib import admin
from .models import Participant, RegisterPaymentInfo
from .forms import RegisterPaymentInfoForm
from django.conf import settings
# Register your models here.


class RegisterPaymentInfoInline(admin.StackedInline):

    model = RegisterPaymentInfo
    extra = 0
    form = RegisterPaymentInfoForm
    fields = ('get_price', 'transaction_date', 'card_number',
            'card_number_last_four', 'transaction_id',
            'card_country', 'card_country_code', 'card_brand',
            'card_type', 'category_name', 'card_bank',
            'card_bank_country', 'card_bank_country_code',
            'client_ip')
    readonly_fields = ('get_price', 'transaction_date', 'card_number',
                       'card_number_last_four', 'transaction_id',
                       'card_country', 'card_country_code', 'card_brand',
                       'card_type', 'category_name', 'card_bank',
                       'card_bank_country', 'card_bank_country_code',
                       'client_ip')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all':  (settings.STATIC_URL + '/admin/css/participant-admin.css',)
        }

    inlines = [RegisterPaymentInfoInline]

    search_fields = ['first_name', 'last_name', 'last_name', 'document',
                     'email', 'confirmation_code']
    list_display = ["first_name", "last_name", "email", "phone", "country",
                    "city", "document", "profession", "speciality",
                    "is_partner", "confirmation_code"]
    list_filter = ["is_partner", "country", "city"]

    fields = ("first_name", "last_name", "email", "phone",
              "country", "city", "document", "profession", "speciality",
              "is_partner", "confirmation_code")

    readonly_fields = ("first_name", "last_name", "email",
                       "phone", "country", "city", "document", "profession",
                       "speciality", "address", "is_partner",
                       "confirmation_code")

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return False
        else:
            return True

    def has_add_permission(self, request):
        if request.user.is_superuser:
            return False

