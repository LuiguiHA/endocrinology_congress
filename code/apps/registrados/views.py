from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, Http404
from django.contrib import messages
from django.conf import settings
from django.utils.translation import activate, check_for_language, \
     LANGUAGE_SESSION_KEY
from .models import Country, City, Partners
from .utils import ValidateCulqi, ParticipantResponse, AmountResponse
from django.utils.translation import gettext as _
from django.db import connection
import uuid
import json
import culqipy
import os

# Create your views here.
culqipy.public_key = os.environ['CULQI_PUBLIC_KEY']
culqipy.secret_key = os.environ['CULQI_PRIVATE_KEY']


class IndexView(View):
    template_name = "index.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        title = _('Inicio - SLEP2018')
        context = {'menu_index': 1, 'title': title}

        return render(request, self.template_name, context)


class RegisterView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        token = data.get('token')
        participant = data.get('participant')
        response = ValidateCulqi(token, participant, request)
        return JsonResponse(response)


class PaymentResponseView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PaymentResponseView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        is_partner = data.get('isPartner')
        amount = AmountResponse(is_partner)
        amount = '{:.2f}'.format(amount)
        return JsonResponse({'amount': amount})


class PartnerResponseView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PartnerResponseView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        name = data.get("name")
        name = name.upper().strip()
        last_name = data.get("lastName")
        last_name = last_name.upper().strip()
        partner_exists = Partners.objects.filter(name=name, last_name=last_name).exists()
        if partner_exists:
            partner = Partners.objects.get(name=name, last_name=last_name)
            if partner.used:
                return JsonResponse({'isPartner': True, 'used': True})
            else:
                return JsonResponse({'isPartner': True, 'used': False})
        else:
            return JsonResponse({'isPartner': False})
        # name = data.get('name')
        # name = name.lower().replace(" ", '')
        # last_name = data.get("lastName")
        # last_name = last_name.lower().replace(" ", '')
        # with connection.cursor() as cursor:
        #     cursor.execute(
        #         """SELECT used from registrados_partners
        #             WHERE LOWER(REPLACE(name, ' ', '')) = %s AND
        #                   LOWER(REPLACE(last_name, ' ', '')) = %s""",
        #         [name, last_name]
        #     )
        #     db_all_rows = cursor.fetchall()
        #
        # if db_all_rows:
        #     used = db_all_rows[0][0]
        #     if used:
        #         return JsonResponse({'isPartner': True, 'used': True})
        #     else:
        #         return JsonResponse({'isPartner': True, 'used': False})
        # else:
        #     return JsonResponse({'isPartner': False})


class ProgramView(View):
    template_name = "program.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ProgramView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        title = _('Programa - SLEP2018')
        context = {'menu_index': 2, 'title': title}
        return render(request, self.template_name, context)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        return JsonResponse(True)


class InscriptionView(View):
    template_name = "inscription.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(InscriptionView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        title = _('Inscripción - SLEP2018')
        context = {'menu_index': 3, 'title': title}
        return render(request, self.template_name, context)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        return JsonResponse(True)


class LanguageView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LanguageView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        raise Http404()

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        language_code = data.get('language_code')
        request.session[LANGUAGE_SESSION_KEY] = language_code
        activate(language_code)
        return JsonResponse({'success': True})


class CountriesView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CountriesView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        countries = list(Country.objects.all().values('name', 'id', 'code'))
        return JsonResponse({'countries': countries, 'success': True})

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        country_id = data.get('country_id')
        country = Country.objects.get(id=country_id)
        cities = list(City.objects.filter(country=country).values('name', 'id'))
        return JsonResponse({'cities': cities, 'success': True})


class VenueView(View):
    template_name = "venue.html"

    def get(self, request, *args, **kwargs):
        title = _('Venue - SLEP2018')
        context = {'menu_index': 5, 'title': title}
        return render(request, self.template_name, context)
