from django.contrib import admin
from .models import HotelReservation, Hotel, HotelRoom, HotelImage
from apps.registrados.models import RegisterPaymentInfo
from apps.registrados.forms import RegisterPaymentInfoForm
# Register your models here.

class HotelRoomInline(admin.StackedInline):

    model = HotelRoom
    extra = 0

class HotelImageInline(admin.StackedInline):

    model = HotelImage
    extra = 0

@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    inlines = [HotelRoomInline, HotelImageInline]
    search_fields = ['name', 'name_en', 'link']
    list_display = ['name', 'name_en', 'link']
    list_filter = ['name', 'name_en']
    
    fields = ['name', 'name_en', 'description', 'description_en', 'link', 'star', 'photo']

    # def has_delete_permission(self, request, obj=None):
    #     return True

    # def has_add_permission(self, request):
    #     return True
    
    # def has_change_permission(self, request, obj=None):
    #     return True
        

class RegisterPaymentInfoInline(admin.StackedInline):

    model = RegisterPaymentInfo
    extra = 0
    form = RegisterPaymentInfoForm
    fields = ('get_price','transaction_date', 'card_number',
            'card_number_last_four', 'transaction_id',
            'card_country', 'card_country_code', 'card_brand',
            'card_type', 'category_name', 'card_bank',
            'card_bank_country', 'card_bank_country_code',
            'client_ip')
    readonly_fields = ('get_price', 'transaction_date', 'card_number',
                       'card_number_last_four', 'transaction_id',
                       'card_country', 'card_country_code', 'card_brand',
                       'card_type', 'category_name', 'card_bank',
                       'card_bank_country', 'card_bank_country_code',
                       'client_ip')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(HotelReservation)
class HotelReservationAdmin(admin.ModelAdmin):
    inlines = [RegisterPaymentInfoInline]
    search_fields = ['name', 'last_name', 'document', 'email']
    list_display = ['name', 'last_name', 'document', 'document_type', 'email', 'hotel_name', 'room_name', 'created_at']
    list_filter = ['document_type', 'hotel_name', 'created_at']

    fields = ['name', 'last_name', 'document', 'document_type', 'email', 'phone', 'hotel_name', 'room_name', 'room_price_formated','days', 'reservation_date']

    readonly_fields = ['name', 'last_name', 'document', 'document_type', 'email', 'phone', 'hotel_name', 'room_name', 'room_price_formated', 'days', 'reservation_date']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False