import json
import culqipy
import arrow
from django.utils.translation import gettext as _
from dateutil import tz
from datetime import datetime, date
from .models import Hotel, HotelRoom, HotelReservation
from apps.registrados.models import RegisterPaymentInfo
from django.template.loader import render_to_string
from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import LANGUAGE_SESSION_KEY


def ValidateCulqi(token, client, request):
    hotel_room = HotelRoom.objects.get(id=client.get('roomId'))
    price = hotel_room.price * int(client.get('days'))
    charge_dict = {
        'card_brand': token.get('card_brand'),
        'amount': int(price * 100),
        'currency_code': 'USD',
        'description': 'Registro',
        'email': token.get('email'),
        'installments': 0,
        'source_id': token.get('id'),
        'antifraud_details': {
            'first_name' : client.get('name'),
            'last_name' : client.get('lastName'),
            'phone_number' : str(client.get('phone'))
        },
        'metadata': {
            'Documento': client.get('document')
        }
    }
    charge = culqipy.Charge.create(charge_dict)
    if charge.get('object') == 'charge':
        reservation = HotelReservation()
        reservation.name = client.get('name').capitalize()
        reservation.last_name = client.get('lastName').capitalize()
        document_types = ['DNI', 'PASAPORTE', 'C.E']
        reservation.document_type = document_types[client.get("documentType") - 1]
        reservation.document = client.get("document")
        reservation.phone = client.get("phone")
        reservation.email = client.get("email")
        reservation.hotel_name = hotel_room.hotel.name
        reservation.room_name = hotel_room.name
        reservation.room_price = hotel_room.price
        reservation.total_price = price
        reservation.since = client.get('since')
        reservation.until = client.get('until')
        reservation.days = int(client.get('days'))
        reservation.save()

        register = RegisterPaymentInfo()
        register.amount = price
        register.transaction_id = charge.get('id')
        creation_date = int(charge.get('creation_date')) / 1000
        creation_date = datetime.fromtimestamp(creation_date).strftime('%Y-%m-%d %H:%M:%S')
        register.transaction_date = creation_date
        register.card_number = charge.get('source').get('card_number')
        register.card_number_last_four = charge.get('source').get('last_four')
        register.card_country = charge.get('source').get('client').get('ip_country')
        register.card_country_code = charge.get('source').get('client').get('ip_country_code')
        register.card_brand = charge.get('source').get('iin').get('card_brand')
        register.card_type = charge.get('source').get('iin').get('card_type')
        register.card_category = charge.get('source').get('iin').get('card_category')
        register.card_bank = charge.get('source').get('iin').get('issuer').get('name')
        register.card_bank_country = charge.get('source').get('iin').get('issuer').get('country')
        register.card_bank_country_code = charge.get('source').get('iin').get('issuer').get('country_code')
        register.client_ip = charge.get('source').get('client').get('ip')
        register.reservation = reservation
        register.save()
        EmailConfirmation(reservation, hotel_room, register, request)
        reservation_response = ReservationResponse(reservation, hotel_room, request)
        return {'success': True, 'reservation': reservation_response}
    else:
        message = _("Error en el pago (code: 1000)")
        if charge.get('type') == 'parameter_error':
            message = _("Error de parámetro de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1001)")

        if charge.get('type') == 'invalid_request_error':
            message = _("Ocurrió un error inesperado. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1002)")

        if charge.get('type') == 'authentication_error':
            message = _("Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1003)")

        if charge.get('type') == 'limit_api_error':
            message = _("Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)")

        if charge.get('type') == 'resource_error':
            message = _("Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)")

        if charge.get('type') == 'api_error':
            message = _("Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1007)")

        if charge.get('type') == 'card_error':

            if charge.get('decline_code'):

                if charge.get('decline_code') == 'insufficient_funds':
                    message = _("Fondos insuficientes. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2001)")

                if charge.get('decline_code') == 'expired_card':
                    message = _("Tarjeta vencida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2002)")

                if charge.get('decline_code') == 'stolen_card':
                    message = _("La tarjeta ha sido reportada como perdida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2004)")

                if charge.get('decline_code') == 'contact_issuer':
                    message = _("El banco ha rechazado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2005)")

                if charge.get('decline_code') == 'invalid_cvv':
                    message = _("Código CVV inválido. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2006)")

                if charge.get('decline_code') == 'incorrect_cvv':
                    message = _("Código CVV incorrecto. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2007)")

                if charge.get('decline_code') == 'too_many_attempts_cvv':
                    message = _("Demasiados intentos para ingresar el código CVV. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2008)")

                if charge.get('decline_code') == 'issuer_not_available':
                    message = _("El banco no responde, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2009)")

                if charge.get('decline_code') == 'issuer_decline_operation':
                    message = _("El banco ha denegado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2010)")

                if charge.get('decline_code') == 'invalid_card':
                    message = _("La tarjeta no está permitida para este tipo de transacciones, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2011)")

                if charge.get('decline_code') == 'processing_error':
                    message = _("Ocurrió un error inesperado mientras se procesaba el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2012)")

                if charge.get('decline_code') == 'fraudulent':
                    message = _("Transacción sospechosa, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2013)")

                if charge.get('decline_code') == 'culqi_card':
                    message = _("Tarjeta no permitida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2014)")

            if charge.get('code'):

                if charge.get('code') == 'card_declined':
                    message = _("Tarjeta rechazada. (code: 2000)")
                else:
                    message = _("Ocurrío un error con la tarjeta (code: 2015)")

        return {'success': False, 'message': message}

def EmailConfirmation(reservation, hotel_room, register, request):
    language = request.LANGUAGE_CODE
    email = reservation.email
    formated_price = "{:.2f}".format(reservation.total_price)
    formated_room_price = "{:.2f}".format(reservation.room_price)
    if reservation.since == reservation.until:
        range_reserved = reservation.since
    else:
        range_reserved = str(reservation.since) + ' - ' + (reservation.until)
    email_template = render_to_string(
        "emails/confirm_reserved_{}.html".format(language),
        {
            'hotel_room': hotel_room,
            'name': reservation.name,
            'last_name': reservation.last_name,
            'formated_room_price': formated_room_price,
            'days': reservation.days,
            'range': range_reserved,
            'formated_price': formated_price,
            'transaction_date': register.transaction_date,
            'transaction_id': register.transaction_id
        }
    )

    email_template_operator = render_to_string(
        "emails/confirm_reserved_operator_{}.html".format(language),
        {
            'reservation': reservation,
            'hotel_room': hotel_room,
            'formated_room_price': formated_room_price,
            'formated_price': formated_price,
            'days': reservation.days,
            'range': range_reserved,
            'transaction_date': register.transaction_date,
            'transaction_id': register.transaction_id
        }
    )

    subject = 'Confirmación de Reserva de Hotel al XXVII Congreso Latinoamericano de Endocrinología Pediátrica'

    if language == "en":
        subject = 'Hotel Reservation confirmation to XXVII Latin American Congress of Pediatric Endocrinology'
    
    send_mail(
        subject,
        '',
        settings.DEFAULT_FROM_EMAIL,
        [email],
        fail_silently=False,
        html_message=email_template,
    )
    send_mail(
        subject,
        '',
        settings.DEFAULT_FROM_EMAIL,
        [settings.OPERATOR_EMAIL],
        fail_silently=False,
        html_message=email_template_operator,
    )


def ReservationResponse(reservation, hotel_room, request):
    register = RegisterPaymentInfo.objects.get(reservation=reservation)
    language = request.LANGUAGE_CODE
    if language == 'es':
        hotel_name = hotel_room.hotel.name
        room_name = hotel_room.name
    if language == 'en':
        hotel_name = hotel_room.hotel.name_en
        room_name = hotel_room.name_en

    reservation_dict = {
        'email': reservation.email,
        'hotel_name': hotel_name,
        'room_name': room_name,
        'amount': '$' + '{:.2f}'.format(reservation.total_price),
        'since': reservation.since,
        'until': reservation.until,
        'days': reservation.days,
        'transaction_id': register.transaction_id,
        'transaction_date': arrow.get(
            register.transaction_date, tz.gettz('America/Lima')
            ).shift(hours=-5).format('YYYY-MM-DD HH:mm:ss')
    }
    return reservation_dict