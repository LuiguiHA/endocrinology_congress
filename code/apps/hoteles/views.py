from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import send_mail
from django.utils.encoding import force_bytes, force_text
from abstract.tokens import account_activation_token
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.translation import gettext as _
from hashids import Hashids
from apps.registrados.models import City, Country
from .models import Hotel, HotelImage, HotelRoom
from .utils import ValidateCulqi
from django.urls import reverse
import uuid
import json
import culqipy
import os
import random
# Create your views here.


class HotelsView(View):
    template_name = "hotel.html"
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(HotelsView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        title = _('Hoteles - SLEP2018')
        language = request.LANGUAGE_CODE
        if language == 'es':
            hotels = list(Hotel.objects.all().order_by('-star').values('id', 'name', 'star', 'description', 'link'))
        if language == 'en':
            hotels = list(Hotel.objects.all().order_by('-star').values('id', 'name_en', 'star', 'description_en', 'link'))
            for hotel in hotels:
                hotel['name'] = hotel.pop('name_en')
                hotel['description'] = hotel.pop('description_en')
        
        for hotel in hotels:
            hotel['roomSelected'] = None
            hotel['reservationRange'] = None
            hotel_object = Hotel.objects.get(id=hotel['id'])
            # IMAGES OF HOTEL
            images = list(HotelImage.objects.filter(hotel=hotel_object))
            hotel_images = [{'photo': hotel_object.photo.url}]
            for image in images:
                hotel_images.append({'photo': image.photo.url})
            hotel['images'] = hotel_images
            #  ROOMS OF HOTEL
            rooms = list(HotelRoom.objects.filter(hotel=hotel_object).order_by('id'))
            hotel_rooms = []
            for room in rooms:
                dic = {}
                dic['id'] = room.id
                if language == 'es':
                    name = room.name
                if language == 'en':
                    name = room.name_en
                dic['name'] = name
                dic['price'] = '$' + '{:.2f}'.format(room.price)
                hotel_rooms.append(dic)
            hotel['rooms'] = hotel_rooms

        context = {'menu_index': 6, 'title': title, 'hotels': json.dumps(hotels)}
        return render(request, self.template_name, context)


class HotelDetailView(View):
    template_name = "hotel-detail.html"
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(HotelDetailView, self).dispatch(request, *args, **kwargs)

    def get(self, request, slug, *args, **kwargs):
        hotel = Hotel.objects.get(slug=slug)
        language = request.LANGUAGE_CODE
        if language == 'es':
            name = hotel.name
            description = hotel.description
        if language == 'en':
            name = hotel.name_en
            description = hotel.description_en
        hotel_info = {}
        hotel_info['id'] = hotel.id
        hotel_info['name'] = name
        hotel_info['description'] = description
        hotel_info['link'] = hotel.link
        
        images = list(HotelImage.objects.filter(hotel=hotel))
        hotel_images = [{'photo': hotel.photo.url}]
        for image in images:
            hotel_images.append({'photo': image.photo.url})

        rooms = list(HotelRoom.objects.filter(hotel=hotel).order_by('id'))
        hotel_rooms = []
        for room in rooms:
            dic = {}
            dic['id'] = room.id
            if language == 'es':
                name = room.name
            if language == 'en':
                name = room.name_en
            dic['name'] = name
            dic['price'] = '$' + '{:.2f}'.format(room.price)
            hotel_rooms.append(dic)

        title = _('Hoteles - SLEP2018')
        context = {'title': title, 'menu_index': 6, 'hotel_info': json.dumps(hotel_info), 'hotel_images': hotel_images,
                   'hotel_rooms': hotel_rooms}
        return render(request, self.template_name, context)


class HotelReservedView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(HotelReservedView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        token = data.get('token')
        client = data.get('client')
        response = ValidateCulqi(token, client, request)
        return JsonResponse(response)
    

class PriceReservedView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PriceReservedView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        room = HotelRoom.objects.get(id=data.get('id'))
        price = room.price * int(data.get('days'))
        price = '$' + '{:.2f}'.format(price)
        return JsonResponse({'success': True, 'price': price})