from django.db import models
from stdimage.models import StdImageField
from django.template import defaultfilters

# Create your models here.
class Hotel(models.Model):
    name = models.CharField(
        max_length = 300,
        verbose_name="Nombre"
    )
    name_en = models.CharField(
        max_length = 300,
        verbose_name="Nombre (inglés)"
    )
    description = models.TextField(
        verbose_name="Descripción"
    )
    description_en = models.TextField(
        verbose_name="Descripción (inglés)"
    )
    photo = StdImageField(
        upload_to='hotels',
        verbose_name="Imagen",
        variations={
            'large': (600, 400),
            'medium': (300, 200),
            'small': (100, 100)
        }
    )
    link = models.CharField(
        max_length=500,
        null=True,
        blank=True,
        verbose_name="URL"
    ) 
    slug = models.SlugField()

    star = models.IntegerField(
        verbose_name="Nº de Estrellas"
    )

    def save(self, *args, **kwargs):
      self.slug = defaultfilters.slugify(self.name)
      super(Hotel, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta():
        verbose_name = "Hotel"
        verbose_name_plural = "Hoteles"

class HotelImage(models.Model):
    photo = StdImageField(
        upload_to='hotels-images',
        verbose_name="Imagen",
        variations={
            'large': (600, 400),
            'medium': (300, 200),
            'small': (100, 100)
        }
    )
    hotel = models.ForeignKey(
        Hotel
    )

    def __str__(self):
        return ''

    class Meta():
        verbose_name = "Imagen"
        verbose_name_plural = "Imágenes"

class HotelRoom(models.Model):
    name = models.CharField(
        max_length = 300,
        verbose_name="Nombre"
    )

    name_en = models.CharField(
        max_length = 300,
        verbose_name="Nombre (inglés)"
    )

    price = models.FloatField(
        verbose_name="Precio"
    )

    hotel = models.ForeignKey(
        Hotel
    )
    def __str__(self):
        return ''

    class Meta():
        verbose_name = "Habitación"
        verbose_name_plural = "Habitaciones"

class HotelReservation(models.Model):
    name = models.CharField(
        max_length = 50,
        verbose_name="Nombres"
    )

    last_name = models.CharField(
        max_length = 50,
        verbose_name="Apellidos"
    )
    
    document_type = models.CharField(
        max_length = 50,
        verbose_name="Tipo de documento"
    )

    document = models.CharField(
        max_length = 50,
        verbose_name="N° de documento"
    )

    phone = models.CharField(
        max_length = 50,
        verbose_name="Teléfono"
    )

    email = models.EmailField(
        verbose_name="Correo"
    )

    hotel_name = models.CharField(
        max_length = 300,
        verbose_name="Hotel"
    )

    room_name = models.CharField(
        max_length = 300,
        verbose_name="Habitación"
    )

    room_price = models.FloatField(
        verbose_name="Precio de habitación"
    )
    
    total_price = models.FloatField(
        verbose_name="Precio Total"
    )

    days = models.IntegerField(
        verbose_name="Días de Reserva"
    )

    since = models.DateField(
        verbose_name="Desde"
    )

    until = models.DateField(
        verbose_name="Hasta"
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name="Fecha de reserva"
    )

    def __str__(self):
        return ''
    
    def room_price_formated(self):
        return '$' + '{:.2f}'.format(self.room_price)
    room_price_formated.short_description="Precio de Habitación"

    def reservation_date(self):
        if self.since == self.until:
            return self.since
        else:
            return str(self.since) + '  -  ' + str(self.until)
    reservation_date.short_description="Fechas de Reserva"

    class Meta():
        verbose_name = "Reserva"
        verbose_name_plural = "Reservas"