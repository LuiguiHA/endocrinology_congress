# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-04-17 22:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hoteles', '0006_hotel_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='hotelReservation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('document_type', models.CharField(max_length=50)),
                ('document', models.CharField(max_length=50)),
                ('phone', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('hotel_name', models.CharField(max_length=100)),
                ('room_name', models.CharField(max_length=100)),
                ('room_price', models.FloatField()),
            ],
        ),
    ]
