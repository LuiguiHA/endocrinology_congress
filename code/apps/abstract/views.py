from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import send_mail
from django.utils.encoding import force_bytes, force_text
from abstract.tokens import account_activation_token
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.translation import gettext as _
from hashids import Hashids
from .models import Abstracts, CategoryAbstract, TypeAbstract, StateAuthorAbstract, AuthorAbstract, AuthorAffiliation
from apps.registrados.models import City, Country
from django.urls import reverse
import uuid
import json
import arrow
import culqipy
import os
import random
from zipfile import ZipFile
from django.http import HttpResponse
import io
from easy_pdf.rendering import render_to_pdf
from django.utils.text import slugify
# Create your views here.


class AbstractListView(View):
    template_name = "abstract.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AbstractListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        title = _('Abstracts - SLEP2018')
        abstracts = []
        if request.user.is_authenticated:
            abstracts = Abstracts.objects.filter(creator=request.user).order_by('id')
        
        context = {'menu_index': 4, 'title': title, 'abstracts': abstracts}
        return render(request, self.template_name, context)


class LoginView(View):
    template_name = 'login.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        email = data.get('email')
        password = data.get('password')
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False})


class LogoutView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LogoutView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        pass

    def post(self, request, *args, **kwargs):
        print("asdasd")
        logout(request)
        return JsonResponse({'success': True})


class Activate(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        # except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        except Exception as e:
            print("error validating account ==>", e)
            user = None

        if user is not None and  \
           account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            login(request, user)
            return redirect('abstract_list')
        else:
            return redirect('index')


class RegisterAuthorView(View):
    template_name = 'register-author.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterAuthorView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        title = _('Registro de Autor - SLEP 2018')
        context = {'title': title}
        return render(request, self.template_name, context)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        email = data.get('email')
        email = email.strip()
        user_exists = User.objects.filter(email=email).exists()
        if user_exists:
            return JsonResponse({'success': False})
        else:
            hashids = Hashids(min_length=6)
            first_name = data.get('name').capitalize()
            username = first_name + \
                hashids.encode(random.randrange(0, 101, 2)).upper()
            last_name = data.get('lastName').capitalize()
            password = data.get('password')
            user = User.objects.create(email=email,
                                       first_name=first_name,
                                       last_name=last_name,
                                       username=username,
                                       is_superuser=False,
                                       is_active=False,
                                       is_staff=False)
            user.set_password(password)
            user.save()

            current_site = get_current_site(request)
            token = account_activation_token.make_token(user)
            uid = urlsafe_base64_encode(force_bytes(user.pk))
            language_code = request.session.get(settings.LANGUAGE_SESSION_KEY,  'es')
            email_template = render_to_string(
                "emails/confirm_registration_{}.html".format(language_code),
                # "emails/confirm_registration.html",
                { 
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                }
            )
            send_mail(
                'Slep2018 - Confirmación de correo',
                'Texto plano',
                settings.DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
                html_message=email_template,
            )
            return JsonResponse({'success': True})


class AbstractView(View):
    template_name = 'register-abstract.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AbstractView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET
        current = None
        
        if data.get('id'):
            exists_abstract = Abstracts.objects.filter(id=data.get('id'), creator=request.user).exists()
            if exists_abstract:
                abstract = Abstracts.objects.get(id=data.get('id'))
                current = {}
                current['title'] = abstract.title
                current['info'] = abstract.info
                current['approval'] = abstract.approval
                current['helsinki'] = abstract.helsinki
                current['animal'] = abstract.animal
                current['pacient'] = abstract.pacient
                current['ilegal'] = abstract.ilegal
                current['previousPublication'] = abstract.previousPublication
                current['previousSubmission'] = abstract.previousSubmission
                current['copyright'] = abstract.copyright
                current['copyrightCanceled'] = abstract.copyrightCanceled
                current['guidelines'] = abstract.guidelines
                current['conflict'] = abstract.conflict
                current['conflictReason'] = abstract.conflictReason
                current['funding'] = abstract.funding
                current['fundingReason'] = abstract.fundingReason
                current['clinicalCase'] = abstract.clinicalCase
                current['presentation'] = abstract.presentation
                # current['birthAuthor'] = abstract.birthAuthor
                # current['grant'] = abstract.grant
                # current['grantEspe'] = abstract.grantEspe
                # current['member'] = abstract.member
                current['type'] = abstract.type.id
                current['category'] = abstract.category.id
                # current['stateAuthor'] = abstract.state_author.id
                current['authors'] = []
                authors = list(AuthorAbstract.objects.filter(abstract=abstract).order_by('id'))
                for author in authors:
                    dic = {}
                    dic['name'] = author.firstname
                    dic['lastName'] = author.lastname
                    dic['presentation'] = author.presentation
                    dic['affiliations'] = []
                    affiliations = list(AuthorAffiliation.objects.filter(author=author).order_by('id'))
                    for affiliation in affiliations:
                        dicAffilation = {}
                        dicAffilation['institution'] = affiliation.institution
                        country = Country.objects.get(name=affiliation.country)
                        dicAffilation['country'] = country.id
                        dicAffilation['city'] = affiliation.city
                        dic['affiliations'].append(dicAffilation)
                    current['authors'].append(dic)
            else:
                return redirect(reverse('abstract_list'))

        title = _('Envío de Abstract - SLEP2018')
        context = {'title': title, 'current': json.dumps(current)}
        return render(request, self.template_name, context)
    
    # def post(self, request, *args, **kwargs):
        # data = json.loads(request.body)
        # abstract = Abstracts()
        # abstract.language = request.LANGUAGE_CODE
        # abstract.creator = request.user
        # abstract.state = 'PENDIENTE'
        # abstract.state_en = 'PENDING'
        # abstract.title = data.get('title')
        # abstract.info = data.get('info')
        # abstract.approval = data.get('approval')
        # abstract.helsinki = data.get('helsinki')
        # abstract.animal = data.get('animal')
        # abstract.pacient = data.get('pacient')
        # abstract.ilegal = data.get('ilegal')
        # abstract.previousPublication = data.get('previousPublication')
        # abstract.previousSubmission = data.get('previousSubmission')
        # abstract.copyright = data.get('copyright')
        # abstract.copyrightCanceled = data.get('copyrightCanceled')
        # abstract.guidelines = data.get('guidelines')
        # abstract.conflict = data.get('conflict')
        # abstract.conflictReason = data.get('conflictReason')
        # abstract.funding = data.get('funding')
        # abstract.fundingReason = data.get('fundingReason')
        # abstract.clinicalCase = data.get('clinicalCase')
        # if data.get('presentation'):
        #     abstract.presentation = data.get('presentation')
        # # if data.get('birthAuthor'):
        # #     abstract.birthAuthor = data.get('birthAuthor')
        # # if data.get('grant'):
        # #     abstract.grant = data.get('grant')
        # # abstract.grantEspe = data.get('grantEspe')
        # # if data.get('member'):
        # #     abstract.member = data.get('member')
        # category = CategoryAbstract.objects.get(id=data.get('category'))
        # abstract.category = category
        # type_abstract = TypeAbstract.objects.get(id=data.get('type')) 
        # abstract.type = type_abstract
        # # state_author = StateAuthorAbstract.objects.get(id=data.get('stateAuthor'))
        # # abstract.state_author = state_author
        # abstract.save()
        # # AUTORES
        # authors = data.get('authors')
        # for author in authors:
        #     author_object = AuthorAbstract()
        #     author_object.firstname = author.get('name')
        #     author_object.lastname = author.get('lastName')
        #     author_object.presentation = author.get('presentation')
        #     author_object.abstract = abstract
        #     author_object.save()
        #     affiliations = author.get('affiliations')
        #     for affiliation in affiliations:
        #         affiliation_object = AuthorAffiliation()
        #         affiliation_object.institution = affiliation.get('institution')
        #         country = Country.objects.get(id=affiliation.get('country'))
        #         affiliation_object.country = country.name
        #         if isinstance(affiliation.get('city'), int):
        #             city = City.objects.get(id=affiliation.get('city'))
        #             affiliation_object.city = city.name
        #         else:
        #             affiliation_object.city = affiliation.get('city')
        #         affiliation_object.author = author_object
        #         affiliation_object.save()

        # language = abstract.language
        # email_template = render_to_string(
        #     "emails/reception_abstract_{}.html".format(language),
        #     {
        #         'title': abstract.title,
        #         'name': abstract.creator.get_full_name()
        #     }
        # )
        # if language == 'es':
        #     subject = 'Recepción de Abstract - SLEP 2018'
        # else:
        #     subject = 'Abstract Reception - SLEP 2018'

        # send_mail(
        #     subject,
        #     '',
        #     settings.DEFAULT_FROM_EMAIL,
        #     [abstract.creator.email],
        #     fail_silently=False,
        #     html_message=email_template,
        # )
        # return JsonResponse({'success': True})

    def post(self, request, *args, **kwargs):
        return JsonResponse({'success': False})

class CategoriesAbstractView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CategoriesAbstractView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.LANGUAGE_CODE == 'es':
            categories = list(CategoryAbstract.objects.all().values('name', 'id'))
        else:
            categories = list(CategoryAbstract.objects.all().values('name_en', 'id'))
            for category in categories:
                category['name'] = category.pop('name_en')
        return JsonResponse({'categories': categories, 'success': True})
        

class TypesAbstractView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(TypesAbstractView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.LANGUAGE_CODE == 'es':
            types = list(TypeAbstract.objects.all().values('name', 'id'))
        else:
            types = list(TypeAbstract.objects.all().values('name_en', 'id'))
            for type_abstract in types:
                type_abstract['name'] = type_abstract.pop('name_en')
        
        return JsonResponse({'types': types, 'success': True})
        

class StateAuthorAbstractView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(StateAuthorAbstractView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        states = list(StateAuthorAbstract.objects.all().values('name', 'id'))
        return JsonResponse({'states': states, 'success': True})


class AbstractConfirmView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AbstractConfirmView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        abstract = Abstracts.objects.get(id=data.get('id'))
        abstract.state = 'CONFIRMADO'
        abstract.state_en = 'CONFIRMED'
        abstract.dismissed_reason = None
        abstract.save()
        language = abstract.language
        email_template = render_to_string(
            "emails/confirm_abstract_{}.html".format(language),
            {
                'title': abstract.title,
                'send_date': arrow.get(abstract.published_date).format('DD-MM-YYYY'),
                'name': abstract.creator.get_full_name()
            }
        )
        if language == 'es':
            subject = 'Notificación de estado de Abstract - SLEP 2018'
        else:
            subject = 'Abstract status notification - SLEP 2018'

        send_mail(
            subject,
            '',
            settings.DEFAULT_FROM_EMAIL,
            [abstract.creator.email],
            fail_silently=False,
            html_message=email_template,
        )

        messages.add_message(request, messages.SUCCESS, abstract.title + ' ha sido confirmado exitosamente.')
        return JsonResponse({'success': True})


class AbstractDismissedView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AbstractDismissedView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        abstract = Abstracts.objects.get(id=data.get('id'))
        abstract.state = 'RECHAZADO'
        abstract.state = 'REJECTED'
        abstract.dismissed_reason = data.get('reason')
        abstract.save()
        language = abstract.language
        email_template = render_to_string(
            "emails/dismissed_abstract_{}.html".format(language),
            {
                'title': abstract.title,
                'send_date': arrow.get(abstract.published_date).format('DD-MM-YYYY'),
                'reason': abstract.dismissed_reason,
                'name': abstract.creator.get_full_name()
            }
        )
        if language == 'es':
            subject = 'Notificación de estado de Abstract - SLEP 2018'
        else:
            subject = 'Abstract status notification - SLEP 2018'

        send_mail(
            subject,
            '',
            settings.DEFAULT_FROM_EMAIL,
            [abstract.creator.email],
            fail_silently=False,
            html_message=email_template,
        )
        messages.add_message(request, messages.ERROR, abstract.title + ' ha sido rechazado.')
        return JsonResponse({'success': True})


class DownloadAbstractsView(View):
    template_name = "abstract_pdf.html"
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DownloadAbstractsView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        in_memory = io.BytesIO()
        zip = ZipFile(in_memory, "a")
        abstracts = list(Abstracts.objects.all())
        for abstract in abstracts:
            context = {}
            context['abstract'] = abstract
            context['published_date'] = arrow.get(abstract.published_date).format('DD-MM-YYYY')
            authors_list = []
            authors = list(AuthorAbstract.objects.filter(abstract=abstract).order_by('id'))
            count = 1
            for author in authors:
                dic = {}
                dic['index'] = count
                dic['name'] = author.firstname.title()
                dic['lastname'] = author.lastname.title()
                dic['presentation'] = 'Si' if author.presentation else 'No'
                affiliation_list = []
                affiliations = AuthorAffiliation.objects.filter(author=author)
                for affiliation in affiliations:
                    dic_affiliation = {}
                    dic_affiliation['institution'] = affiliation.institution
                    dic_affiliation['country'] = affiliation.country
                    dic_affiliation['city'] = affiliation.city
                    affiliation_list.append(dic_affiliation)
                dic['affiliations'] = affiliation_list
                authors_list.append(dic)
                count = count + 1

            context['authors'] = authors_list
            pdf = render_to_pdf(self.template_name, context)
            name = abstract.title.replace(' ', '-')
            name = name.replace('/', '')
            name = name + '.pdf'
            zip.writestr(name, pdf)

        for file in zip.filelist:
            file.create_system = 0    
        zip.close()

        response = HttpResponse()
        response["Content-Disposition"] = "attachment; filename=abstracts.zip"
        in_memory.seek(0)    
        response.write(in_memory.read())
        return response