from django.contrib import admin
from django.contrib.auth.models import User, Group
import nested_admin
from django.conf import settings
from .models import Abstracts, AuthorAbstract, AuthorAffiliation
# Register your models here.
class AffiliationInline(nested_admin.NestedStackedInline):
    model = AuthorAffiliation

    fields = ('institution', 'country', 'city')
    readonly_fields = ('institution', 'country', 'city')
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class AuthorInline(nested_admin.NestedStackedInline):
    inlines = [AffiliationInline]
    model = AuthorAbstract

    fields = ("firstname", "lastname", "presentation")
    readonly_fields = ("firstname", "lastname", "presentation")

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.unregister(User)
admin.site.unregister(Group)
@admin.register(Abstracts)
class AbstractAdmin(nested_admin.NestedModelAdmin):
    class Media:
        css = {
            'all':  (settings.STATIC_URL + '/admin/css/abstract-admin.css',)
        }

    inlines = [AuthorInline]
    
    search_fields = ['title']
    list_filter = ["published_date", "state"]
    list_display = ["title","creator_name", "creator_email", "published_date", "state"]
    
    def get_fields(self, request, obj=None):
        fields = ["creator_name", "creator_email", "title", "published_date", "info_format", "state", "dismissed_reason",
              "approval", "helsinki_format", "animal", "pacient",
              "ilegal", "previousPublication", "previousSubmission",
              "copyright", "copyrightCanceled", "guidelines",
              "conflict", "conflictReason_format", "funding",
              "fundingReason_format", "category_name", "type_name",
              "clinicalCase", "presentation_name"]
        if obj.dismissed_reason:
            return fields
        else:
            fields.remove('dismissed_reason')
            return fields

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ["creator_name", "creator_email", "title", "published_date", "info_format", "state", "dismissed_reason",
              "approval", "helsinki_format", "animal", "pacient",
              "ilegal", "previousPublication", "previousSubmission",
              "copyright", "copyrightCanceled", "guidelines",
              "conflict", "conflictReason_format", "funding",
              "fundingReason_format", "category_name", "type_name",
              "clinicalCase", "presentation_name"]
        if obj.dismissed_reason:
            return readonly_fields
        else:
            readonly_fields.remove('dismissed_reason')
            return readonly_fields

    def has_delete_permission(self, request, obj=None):
        return True

    def has_add_permission(self, request):
        return False