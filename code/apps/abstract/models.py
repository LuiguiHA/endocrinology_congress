from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.


class CategoryAbstract(models.Model):
    name = models.CharField(
        max_length = 100
    )
    name_en = models.CharField(
        max_length = 100
    )


class TypeAbstract(models.Model):
    name = models.CharField(
        max_length = 100
    )
    name_en = models.CharField(
        max_length = 100
    )


class StateAuthorAbstract(models.Model):
    name = models.CharField(
        max_length = 300
    )


class Abstracts(models.Model):
    info = models.TextField(
        verbose_name="Abstract"
    )
    creator = models.ForeignKey(
        User
    )
    published_date = models.DateField(
        default=timezone.now,
        verbose_name="Fecha de envío"
    )
    state = models.CharField(
        max_length=50,
        verbose_name="Estado"
    )
    state_en = models.CharField(
        max_length=50,
        verbose_name="Estado"
    )
    title = models.TextField(
        verbose_name="Título"
    )

    approval = models.BooleanField(
        verbose_name="Aprobación del autor"
    )

    helsinki = models.NullBooleanField(
        verbose_name="Declaración de Helsinki"
    )

    animal = models.BooleanField(
        verbose_name="Bienestar de los animales"
    )

    pacient = models.BooleanField(
        verbose_name="Privacidad del paciente"
    )

    ilegal = models.BooleanField(
        verbose_name="Difamatorio o ilegal"
    )

    previousPublication = models.BooleanField(
        verbose_name="Publicación previa"
    )

    previousSubmission = models.BooleanField(
        verbose_name="Presentación anterior"
    )

    copyright = models.BooleanField(
        verbose_name="Derechos de autor"
    )

    copyrightCanceled = models.BooleanField(
        verbose_name="Derechos de autor cancelados"
    )

    guidelines  = models.BooleanField(
        verbose_name="Directrices del autor"
    )

    conflict = models.BooleanField(
        verbose_name="Conflicto de intereses"
    )

    conflictReason = models.TextField()

    funding = models.BooleanField(
        verbose_name="Financiamiento"
    )

    fundingReason = models.TextField()

    clinicalCase = models.BooleanField(
        verbose_name="Caso clínico"
    )

    presentation = models.CharField(
        null=True,
        max_length=50
    )

    # birthAuthor = models.CharField(
    #     max_length=50,
    #     null=True
    # )
    # grant = models.NullBooleanField(
    #     null=True
    # )
    
    # grantEspe = models.BooleanField(
    #     verbose_name="Becas de viaje"
    # )

    # member = models.NullBooleanField()

    category = models.ForeignKey(
        CategoryAbstract
    )
    type = models.ForeignKey(
        TypeAbstract
    )
    # state_author = models.ForeignKey(
    #     StateAuthorAbstract
    # )
    dismissed_reason = models.TextField(
        null=True,
        verbose_name="Motivo de Rechazo"
    )

    language = models.CharField(
        max_length=50
    )

    def creator_name(self):
        return self.creator.first_name.title() + " " + self.creator.last_name.title()
    creator_name.short_description = "Creador"

    def creator_email(self):
        return self.creator.email
    creator_email.short_description = "Correo"

    def helsinki_format(self):
        if self.helsinki == None:
            return '-'
        if self.helsinki == True:
            return 'Si'
        if self.helsinki == False:
            return 'No'
    helsinki_format.short_description = "Declaración de Helsinki"

    def conflictReason_format(self):
        return '-' if self.conflictReason == '' else self.conflictReason
    conflictReason_format.short_description = "Detalle del conflicto de intereses"
    conflictReason_format.allow_tags = True
    
    def fundingReason_format(self):
        return '-' if self.fundingReason == '' else self.fundingReason
    fundingReason_format.short_description = "Detalle del financiamiento"
    fundingReason_format.allow_tags = True

    def category_name(self):
        return self.category.name
    category_name.short_description = "Categoría"

    def type_name(self):
        return self.type.name
    type_name.short_description = "Tipo de Abstract"

    # def state_author_name(self):
    #     return self.state_author.name
    # state_author_name.short_description = "Estado del Autor"

    def presentation_name(self):
        return '-' if self.presentation is None else self.presentation
    presentation_name.short_description = "Presentación del Abstract"
    
    def info_format(self):
        return self.info
    info_format.short_description = "Abstract"
    info_format.allow_tags = True

    # def birthAuthor_name(self):
    #     return '-' if self.birthAuthor is None else self.birthAuthor
    # birthAuthor_name.short_description = "Fecha de nacimiento del Autor"

    # def grant_name(self):
    #     return '-' if self.grant is None else self.grant
    # grant_name.short_description = "solicitante de la ESPE Summer School"

    # def member_name(self):
    #     return '-' if self.member is None else self.member
    # member_name.short_description = "Miembro de ESPE"

    def __str__(self):
        return ''

    class Meta():
        verbose_name = "Abstract"
        verbose_name_plural = "Abstracts"



class AuthorAbstract(models.Model):

    abstract = models.ForeignKey(
        Abstracts
    )

    firstname = models.CharField(
        max_length=50,
        verbose_name="Nombres"
    )

    lastname = models.CharField(
        max_length=50,
        verbose_name="Apellidos"
    )

    presentation = models.BooleanField(
        verbose_name="Presentación"
    )

    def __str__(self):
        return self.firstname

    class Meta():
        verbose_name = "Autor"
        verbose_name_plural = "Autores"

class AuthorAffiliation(models.Model):
    author = models.ForeignKey(
        AuthorAbstract
    )

    institution = models.CharField(
        max_length=500,
        verbose_name="Institución"
    )

    country = models.CharField(
        max_length=100,
        verbose_name="País"
    )
    city = models.CharField(
        max_length=100,
        verbose_name="Ciudad"
    )
    
    def __str__(self):
        return ''

    class Meta():
        verbose_name = "Afiliado"
        verbose_name_plural = "Afiliados"
