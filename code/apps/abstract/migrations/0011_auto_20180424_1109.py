# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-04-24 16:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('abstract', '0010_auto_20180417_1728'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='abstracts',
            name='birthAuthor',
        ),
        migrations.RemoveField(
            model_name='abstracts',
            name='grant',
        ),
        migrations.RemoveField(
            model_name='abstracts',
            name='grantEspe',
        ),
        migrations.RemoveField(
            model_name='abstracts',
            name='member',
        ),
        migrations.RemoveField(
            model_name='abstracts',
            name='state_author',
        ),
    ]
