# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-06-14 20:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('abstract', '0015_abstracts_state_en'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authoraffiliation',
            name='institution',
            field=models.CharField(max_length=500, verbose_name='Institución'),
        ),
    ]
