#!/bin/bash
python manage.py migrate
gunicorn --env DJANGO_SETTINGS_MODULE=project.production_settings project.wsgi \
--timeout 18000 \
--log-level DEBUG
nginx -g 'daemon off;'